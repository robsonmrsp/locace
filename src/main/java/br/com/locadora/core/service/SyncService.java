package br.com.locadora.core.service;

import java.util.List;


import javax.inject.Inject;
import javax.inject.Named;

import br.com.locadora.core.model.SyncInfo;
import br.com.locadora.core.json.*;
import br.com.locadora.core.model.*;
import br.com.locadora.core.persistence.DaoSyncInfo;
import br.com.locadora.core.json.DtoDataBase;

public interface SyncService {
		public DtoDataBase sync(DtoDataBase dataBase) ;
}
