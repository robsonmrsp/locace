package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonAtor;

import br.com.locadora.model.Ator;

import br.com.locadora.service.AtorService;
import br.com.locadora.model.filter.FilterAtor;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:41
**/


@Api("Ator's CRUD API")
@Path("/crud/ators")
public class AtorResources {

	@Inject
	AtorService atorService;
	
	
	public static final Logger LOGGER = Logger.getLogger(AtorResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Ator's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonAtor.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "biografia", value = "Biografia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "data nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterAtor> paginationParams = new PaginationParams<FilterAtor>(uriInfo, FilterAtor.class);

			List<JsonAtor> jsonAtors = Parser.toListJsonAtors(atorService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonAtors).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Ator's List by comparation [LIKE] of query parameters.", response = JsonAtor.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "biografia", value = "Biografia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "data nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterAtor> paginationParams = new PaginationParams<FilterAtor>(uriInfo, FilterAtor.class);

			List<JsonAtor> jsonAtors = Parser.toListJsonAtors(atorService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonAtors).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Ators.", response = JsonAtor.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonAtor> jsonAtors = Parser.toListJsonAtors(atorService.all());
			response = Response.ok(jsonAtors).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Ator's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "biografia", value = "Biografia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "data nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Ator> ators = null;

		try {
			PaginationParams<FilterAtor> paginationParams = new PaginationParams<FilterAtor>(uriInfo, FilterAtor.class);
			ators = atorService.all(paginationParams);
			JsonPaginator<JsonAtor> paginator = new JsonPaginator<JsonAtor>(Parser.toListJsonAtors(ators.getItens()), ators.getActualPage(), ators.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar ators para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Ator ator = atorService.get(id);

			return Response.ok().entity(Parser.toJson(ator)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Ator.", response = JsonAtor.class)
	public Response save(JsonAtor jsonAtor) {
		try {

			Ator ator = Parser.toEntity(jsonAtor);
			ator = atorService.save(ator);
			return Response.ok().entity(Parser.toJson(ator)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonAtor.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonAtor, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  ator [ %s ] parametros [ %s ]", e.getMessage(), jsonAtor.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonAtor)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Ator. with the 'id' ", response = JsonAtor.class)
	public Response update(@PathParam("id") Integer id, JsonAtor jsonAtor) {
		try {
			Ator ator = Parser.toEntity(jsonAtor);

			ator = atorService.save(ator);
			return Response.ok().entity(Parser.toJson(ator)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonAtor.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonAtor, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonAtor.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonAtor)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Ator with the 'id' .", response = JsonAtor.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(atorService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
