package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonReserva;

import br.com.locadora.model.Reserva;

import br.com.locadora.service.ReservaService;
import br.com.locadora.model.filter.FilterReserva;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:42
**/


@Api("Reserva's CRUD API")
@Path("/crud/reservas")
public class ReservaResources {

	@Inject
	ReservaService reservaService;
	
	
	public static final Logger LOGGER = Logger.getLogger(ReservaResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Reserva's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonReserva.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "dataHora", value = "Data e hora", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "cliente", value = "Cliente", paramType = "query", dataType="integer"),		
		@ApiImplicitParam(name = "filme", value = "Filme", paramType = "query", dataType="integer"),		
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterReserva> paginationParams = new PaginationParams<FilterReserva>(uriInfo, FilterReserva.class);

			List<JsonReserva> jsonReservas = Parser.toListJsonReservas(reservaService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonReservas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Reserva's List by comparation [LIKE] of query parameters.", response = JsonReserva.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "dataHora", value = "Data e hora", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "cliente", value = "Cliente", paramType = "query", dataType="integer"),		
		@ApiImplicitParam(name = "filme", value = "Filme", paramType = "query", dataType="integer"),		
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterReserva> paginationParams = new PaginationParams<FilterReserva>(uriInfo, FilterReserva.class);

			List<JsonReserva> jsonReservas = Parser.toListJsonReservas(reservaService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonReservas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Reservas.", response = JsonReserva.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonReserva> jsonReservas = Parser.toListJsonReservas(reservaService.all());
			response = Response.ok(jsonReservas).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Reserva's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "dataHora", value = "Data e hora", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "cliente", value = "Cliente", paramType = "query", dataType="integer"),		
		@ApiImplicitParam(name = "filme", value = "Filme", paramType = "query", dataType="integer"),		
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Reserva> reservas = null;

		try {
			PaginationParams<FilterReserva> paginationParams = new PaginationParams<FilterReserva>(uriInfo, FilterReserva.class);
			reservas = reservaService.all(paginationParams);
			JsonPaginator<JsonReserva> paginator = new JsonPaginator<JsonReserva>(Parser.toListJsonReservas(reservas.getItens()), reservas.getActualPage(), reservas.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar reservas para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Reserva reserva = reservaService.get(id);

			return Response.ok().entity(Parser.toJson(reserva)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Reserva.", response = JsonReserva.class)
	public Response save(JsonReserva jsonReserva) {
		try {

			Reserva reserva = Parser.toEntity(jsonReserva);
			reserva = reservaService.save(reserva);
			return Response.ok().entity(Parser.toJson(reserva)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonReserva.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonReserva, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  reserva [ %s ] parametros [ %s ]", e.getMessage(), jsonReserva.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonReserva)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Reserva. with the 'id' ", response = JsonReserva.class)
	public Response update(@PathParam("id") Integer id, JsonReserva jsonReserva) {
		try {
			Reserva reserva = Parser.toEntity(jsonReserva);

			reserva = reservaService.save(reserva);
			return Response.ok().entity(Parser.toJson(reserva)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonReserva.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonReserva, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonReserva.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonReserva)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Reserva with the 'id' .", response = JsonReserva.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(reservaService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
