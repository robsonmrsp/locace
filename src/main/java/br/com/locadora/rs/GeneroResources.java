package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonGenero;

import br.com.locadora.model.Genero;

import br.com.locadora.service.GeneroService;
import br.com.locadora.model.filter.FilterGenero;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:41
**/


@Api("Genero's CRUD API")
@Path("/crud/generos")
public class GeneroResources {

	@Inject
	GeneroService generoService;
	
	
	public static final Logger LOGGER = Logger.getLogger(GeneroResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Genero's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonGenero.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "descricao", value = "Descrição", paramType = "query", dataType="String"),  			
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterGenero> paginationParams = new PaginationParams<FilterGenero>(uriInfo, FilterGenero.class);

			List<JsonGenero> jsonGeneros = Parser.toListJsonGeneros(generoService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonGeneros).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Genero's List by comparation [LIKE] of query parameters.", response = JsonGenero.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "descricao", value = "Descrição", paramType = "query", dataType="String"),  			
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterGenero> paginationParams = new PaginationParams<FilterGenero>(uriInfo, FilterGenero.class);

			List<JsonGenero> jsonGeneros = Parser.toListJsonGeneros(generoService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonGeneros).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Generos.", response = JsonGenero.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonGenero> jsonGeneros = Parser.toListJsonGeneros(generoService.all());
			response = Response.ok(jsonGeneros).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Genero's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "descricao", value = "Descrição", paramType = "query", dataType="String"),  			
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Genero> generos = null;

		try {
			PaginationParams<FilterGenero> paginationParams = new PaginationParams<FilterGenero>(uriInfo, FilterGenero.class);
			generos = generoService.all(paginationParams);
			JsonPaginator<JsonGenero> paginator = new JsonPaginator<JsonGenero>(Parser.toListJsonGeneros(generos.getItens()), generos.getActualPage(), generos.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar generos para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Genero genero = generoService.get(id);

			return Response.ok().entity(Parser.toJson(genero)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Genero.", response = JsonGenero.class)
	public Response save(JsonGenero jsonGenero) {
		try {

			Genero genero = Parser.toEntity(jsonGenero);
			genero = generoService.save(genero);
			return Response.ok().entity(Parser.toJson(genero)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonGenero.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonGenero, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  genero [ %s ] parametros [ %s ]", e.getMessage(), jsonGenero.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonGenero)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Genero. with the 'id' ", response = JsonGenero.class)
	public Response update(@PathParam("id") Integer id, JsonGenero jsonGenero) {
		try {
			Genero genero = Parser.toEntity(jsonGenero);

			genero = generoService.save(genero);
			return Response.ok().entity(Parser.toJson(genero)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonGenero.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonGenero, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonGenero.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonGenero)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Genero with the 'id' .", response = JsonGenero.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(generoService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
