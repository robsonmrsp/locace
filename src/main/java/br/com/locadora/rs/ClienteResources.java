package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonCliente;

import br.com.locadora.model.Cliente;

import br.com.locadora.service.ClienteService;
import br.com.locadora.model.filter.FilterCliente;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:42
**/


@Api("Cliente's CRUD API")
@Path("/crud/clientes")
public class ClienteResources {

	@Inject
	ClienteService clienteService;
	
	
	public static final Logger LOGGER = Logger.getLogger(ClienteResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Cliente's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonCliente.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "endereco", value = "Endereco", paramType = "query", dataType="integer"),		
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterCliente> paginationParams = new PaginationParams<FilterCliente>(uriInfo, FilterCliente.class);

			List<JsonCliente> jsonClientes = Parser.toListJsonClientes(clienteService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonClientes).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Cliente's List by comparation [LIKE] of query parameters.", response = JsonCliente.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "endereco", value = "Endereco", paramType = "query", dataType="integer"),		
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterCliente> paginationParams = new PaginationParams<FilterCliente>(uriInfo, FilterCliente.class);

			List<JsonCliente> jsonClientes = Parser.toListJsonClientes(clienteService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonClientes).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Clientes.", response = JsonCliente.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonCliente> jsonClientes = Parser.toListJsonClientes(clienteService.all());
			response = Response.ok(jsonClientes).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Cliente's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "endereco", value = "Endereco", paramType = "query", dataType="integer"),		
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Cliente> clientes = null;

		try {
			PaginationParams<FilterCliente> paginationParams = new PaginationParams<FilterCliente>(uriInfo, FilterCliente.class);
			clientes = clienteService.all(paginationParams);
			JsonPaginator<JsonCliente> paginator = new JsonPaginator<JsonCliente>(Parser.toListJsonClientes(clientes.getItens()), clientes.getActualPage(), clientes.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar clientes para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Cliente cliente = clienteService.get(id);

			return Response.ok().entity(Parser.toJson(cliente)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Cliente.", response = JsonCliente.class)
	public Response save(JsonCliente jsonCliente) {
		try {

			Cliente cliente = Parser.toEntity(jsonCliente);
			cliente = clienteService.save(cliente);
			return Response.ok().entity(Parser.toJson(cliente)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCliente.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCliente, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  cliente [ %s ] parametros [ %s ]", e.getMessage(), jsonCliente.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCliente)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Cliente. with the 'id' ", response = JsonCliente.class)
	public Response update(@PathParam("id") Integer id, JsonCliente jsonCliente) {
		try {
			Cliente cliente = Parser.toEntity(jsonCliente);

			cliente = clienteService.save(cliente);
			return Response.ok().entity(Parser.toJson(cliente)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonCliente.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonCliente, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonCliente.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonCliente)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Cliente with the 'id' .", response = JsonCliente.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(clienteService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
