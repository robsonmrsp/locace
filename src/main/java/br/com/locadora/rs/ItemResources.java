package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonItem;

import br.com.locadora.model.Item;

import br.com.locadora.service.ItemService;
import br.com.locadora.model.filter.FilterItem;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:43
**/


@Api("Item's CRUD API")
@Path("/crud/items")
public class ItemResources {

	@Inject
	ItemService itemService;
	
	
	public static final Logger LOGGER = Logger.getLogger(ItemResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Item's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonItem.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "name", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "itemType", value = "Tipo", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "identifier", value = "Identificador", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "description", value = "Descrição", paramType = "query", dataType="String"),  			
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterItem> paginationParams = new PaginationParams<FilterItem>(uriInfo, FilterItem.class);

			List<JsonItem> jsonItems = Parser.toListJsonItems(itemService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonItems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Item's List by comparation [LIKE] of query parameters.", response = JsonItem.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "name", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "itemType", value = "Tipo", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "identifier", value = "Identificador", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "description", value = "Descrição", paramType = "query", dataType="String"),  			
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterItem> paginationParams = new PaginationParams<FilterItem>(uriInfo, FilterItem.class);

			List<JsonItem> jsonItems = Parser.toListJsonItems(itemService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonItems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Items.", response = JsonItem.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonItem> jsonItems = Parser.toListJsonItems(itemService.all());
			response = Response.ok(jsonItems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Item's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "name", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "itemType", value = "Tipo", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "identifier", value = "Identificador", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "description", value = "Descrição", paramType = "query", dataType="String"),  			
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Item> items = null;

		try {
			PaginationParams<FilterItem> paginationParams = new PaginationParams<FilterItem>(uriInfo, FilterItem.class);
			items = itemService.all(paginationParams);
			JsonPaginator<JsonItem> paginator = new JsonPaginator<JsonItem>(Parser.toListJsonItems(items.getItens()), items.getActualPage(), items.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar items para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Item item = itemService.get(id);

			return Response.ok().entity(Parser.toJson(item)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Item.", response = JsonItem.class)
	public Response save(JsonItem jsonItem) {
		try {

			Item item = Parser.toEntity(jsonItem);
			item = itemService.save(item);
			return Response.ok().entity(Parser.toJson(item)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonItem.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonItem, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  item [ %s ] parametros [ %s ]", e.getMessage(), jsonItem.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonItem)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Item. with the 'id' ", response = JsonItem.class)
	public Response update(@PathParam("id") Integer id, JsonItem jsonItem) {
		try {
			Item item = Parser.toEntity(jsonItem);

			item = itemService.save(item);
			return Response.ok().entity(Parser.toJson(item)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonItem.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonItem, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonItem.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonItem)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Item with the 'id' .", response = JsonItem.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(itemService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
