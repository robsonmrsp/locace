package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonFuncionario;

import br.com.locadora.model.Funcionario;

import br.com.locadora.service.FuncionarioService;
import br.com.locadora.model.filter.FilterFuncionario;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:41
**/


@Api("Funcionario's CRUD API")
@Path("/crud/funcionarios")
public class FuncionarioResources {

	@Inject
	FuncionarioService funcionarioService;
	
	
	public static final Logger LOGGER = Logger.getLogger(FuncionarioResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Funcionario's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonFuncionario.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "celular", value = "celular", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "rg", value = "Rg", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataHoraDemissao", value = "Data e Hora da Demissao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataHoraContratacao", value = "Data e Hora da Contratacao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "Data de Nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "salario", value = "Salário", paramType = "query", dataType="Double"),  			
		@ApiImplicitParam(name = "observacao", value = "Observação", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "ativo", value = "Ativo", paramType = "query", dataType="Boolean"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "percentualComissao", value = "Percentual comissao", paramType = "query", dataType="Double"),  			
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterFuncionario> paginationParams = new PaginationParams<FilterFuncionario>(uriInfo, FilterFuncionario.class);

			List<JsonFuncionario> jsonFuncionarios = Parser.toListJsonFuncionarios(funcionarioService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonFuncionarios).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Funcionario's List by comparation [LIKE] of query parameters.", response = JsonFuncionario.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "celular", value = "celular", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "rg", value = "Rg", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataHoraDemissao", value = "Data e Hora da Demissao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataHoraContratacao", value = "Data e Hora da Contratacao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "Data de Nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "salario", value = "Salário", paramType = "query", dataType="Double"),  			
		@ApiImplicitParam(name = "observacao", value = "Observação", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "ativo", value = "Ativo", paramType = "query", dataType="Boolean"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "percentualComissao", value = "Percentual comissao", paramType = "query", dataType="Double"),  			
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterFuncionario> paginationParams = new PaginationParams<FilterFuncionario>(uriInfo, FilterFuncionario.class);

			List<JsonFuncionario> jsonFuncionarios = Parser.toListJsonFuncionarios(funcionarioService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonFuncionarios).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Funcionarios.", response = JsonFuncionario.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonFuncionario> jsonFuncionarios = Parser.toListJsonFuncionarios(funcionarioService.all());
			response = Response.ok(jsonFuncionarios).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Funcionario's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "celular", value = "celular", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "rg", value = "Rg", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "dataHoraDemissao", value = "Data e Hora da Demissao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataHoraContratacao", value = "Data e Hora da Contratacao", paramType = "query", dataType="LocalDateTime"),  			
		@ApiImplicitParam(name = "dataNascimento", value = "Data de Nascimento", paramType = "query", dataType="LocalDate"),  			
		@ApiImplicitParam(name = "salario", value = "Salário", paramType = "query", dataType="Double"),  			
		@ApiImplicitParam(name = "observacao", value = "Observação", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "ativo", value = "Ativo", paramType = "query", dataType="Boolean"),  			
		@ApiImplicitParam(name = "telefone", value = "Telefone", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cpf", value = "Cpf", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "foto", value = "Foto", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "percentualComissao", value = "Percentual comissao", paramType = "query", dataType="Double"),  			
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Funcionario> funcionarios = null;

		try {
			PaginationParams<FilterFuncionario> paginationParams = new PaginationParams<FilterFuncionario>(uriInfo, FilterFuncionario.class);
			funcionarios = funcionarioService.all(paginationParams);
			JsonPaginator<JsonFuncionario> paginator = new JsonPaginator<JsonFuncionario>(Parser.toListJsonFuncionarios(funcionarios.getItens()), funcionarios.getActualPage(), funcionarios.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar funcionarios para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Funcionario funcionario = funcionarioService.get(id);

			return Response.ok().entity(Parser.toJson(funcionario)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Funcionario.", response = JsonFuncionario.class)
	public Response save(JsonFuncionario jsonFuncionario) {
		try {

			Funcionario funcionario = Parser.toEntity(jsonFuncionario);
			funcionario = funcionarioService.save(funcionario);
			return Response.ok().entity(Parser.toJson(funcionario)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonFuncionario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonFuncionario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  funcionario [ %s ] parametros [ %s ]", e.getMessage(), jsonFuncionario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonFuncionario)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Funcionario. with the 'id' ", response = JsonFuncionario.class)
	public Response update(@PathParam("id") Integer id, JsonFuncionario jsonFuncionario) {
		try {
			Funcionario funcionario = Parser.toEntity(jsonFuncionario);

			funcionario = funcionarioService.save(funcionario);
			return Response.ok().entity(Parser.toJson(funcionario)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonFuncionario.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonFuncionario, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonFuncionario.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonFuncionario)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Funcionario with the 'id' .", response = JsonFuncionario.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(funcionarioService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
