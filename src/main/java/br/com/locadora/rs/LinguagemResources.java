package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonLinguagem;

import br.com.locadora.model.Linguagem;

import br.com.locadora.service.LinguagemService;
import br.com.locadora.model.filter.FilterLinguagem;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:42
**/


@Api("Linguagem's CRUD API")
@Path("/crud/linguagems")
public class LinguagemResources {

	@Inject
	LinguagemService linguagemService;
	
	
	public static final Logger LOGGER = Logger.getLogger(LinguagemResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Linguagem's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonLinguagem.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterLinguagem> paginationParams = new PaginationParams<FilterLinguagem>(uriInfo, FilterLinguagem.class);

			List<JsonLinguagem> jsonLinguagems = Parser.toListJsonLinguagems(linguagemService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonLinguagems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Linguagem's List by comparation [LIKE] of query parameters.", response = JsonLinguagem.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterLinguagem> paginationParams = new PaginationParams<FilterLinguagem>(uriInfo, FilterLinguagem.class);

			List<JsonLinguagem> jsonLinguagems = Parser.toListJsonLinguagems(linguagemService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonLinguagems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Linguagems.", response = JsonLinguagem.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonLinguagem> jsonLinguagems = Parser.toListJsonLinguagems(linguagemService.all());
			response = Response.ok(jsonLinguagems).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Linguagem's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "nome", value = "Nome", paramType = "query", dataType="String"),  			
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Linguagem> linguagems = null;

		try {
			PaginationParams<FilterLinguagem> paginationParams = new PaginationParams<FilterLinguagem>(uriInfo, FilterLinguagem.class);
			linguagems = linguagemService.all(paginationParams);
			JsonPaginator<JsonLinguagem> paginator = new JsonPaginator<JsonLinguagem>(Parser.toListJsonLinguagems(linguagems.getItens()), linguagems.getActualPage(), linguagems.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar linguagems para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Linguagem linguagem = linguagemService.get(id);

			return Response.ok().entity(Parser.toJson(linguagem)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Linguagem.", response = JsonLinguagem.class)
	public Response save(JsonLinguagem jsonLinguagem) {
		try {

			Linguagem linguagem = Parser.toEntity(jsonLinguagem);
			linguagem = linguagemService.save(linguagem);
			return Response.ok().entity(Parser.toJson(linguagem)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonLinguagem.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonLinguagem, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  linguagem [ %s ] parametros [ %s ]", e.getMessage(), jsonLinguagem.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonLinguagem)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Linguagem. with the 'id' ", response = JsonLinguagem.class)
	public Response update(@PathParam("id") Integer id, JsonLinguagem jsonLinguagem) {
		try {
			Linguagem linguagem = Parser.toEntity(jsonLinguagem);

			linguagem = linguagemService.save(linguagem);
			return Response.ok().entity(Parser.toJson(linguagem)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonLinguagem.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonLinguagem, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonLinguagem.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonLinguagem)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Linguagem with the 'id' .", response = JsonLinguagem.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(linguagemService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
