package br.com.locadora.rs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import br.com.locadora.core.json.JsonError;
import br.com.locadora.core.json.JsonPaginator;
import br.com.locadora.json.JsonEndereco;

import br.com.locadora.model.Endereco;

import br.com.locadora.service.EnderecoService;
import br.com.locadora.model.filter.FilterEndereco;
import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.service.UserService;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.security.SpringSecurityUserContext;

import br.com.locadora.utils.Parser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
*  generated: 12/06/2017 14:33:40
**/


@Api("Endereco's CRUD API")
@Path("/crud/enderecos")
public class EnderecoResources {

	@Inject
	EnderecoService enderecoService;
	
	
	public static final Logger LOGGER = Logger.getLogger(EnderecoResources.class);

	@GET
	@Path("filterEqual")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Endereco's List by comparation [EXACTLY EQUAL] of query parameters.", response = JsonEndereco.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "referencia", value = "Referencia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "logradouro", value = "logradouro", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cep", value = "Cep", paramType = "query", dataType="integer"),		
	})
	public Response filter(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterEndereco> paginationParams = new PaginationParams<FilterEndereco>(uriInfo, FilterEndereco.class);

			List<JsonEndereco> jsonEnderecos = Parser.toListJsonEnderecos(enderecoService.filter(paginationParams, Boolean.TRUE));
			response = Response.ok(jsonEnderecos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("filterAlike")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Endereco's List by comparation [LIKE] of query parameters.", response = JsonEndereco.class, responseContainer = "List")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "referencia", value = "Referencia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "logradouro", value = "logradouro", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cep", value = "Cep", paramType = "query", dataType="integer"),		
	})
	public Response filterAlike(@Context UriInfo uriInfo) {
		Response response = null;
		try {
			PaginationParams<FilterEndereco> paginationParams = new PaginationParams<FilterEndereco>(uriInfo, FilterEndereco.class);

			List<JsonEndereco> jsonEnderecos = Parser.toListJsonEnderecos(enderecoService.filter(paginationParams, Boolean.FALSE));
			response = Response.ok(jsonEnderecos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e,message, null)).build();
		}
		return response;
	}

	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All the Enderecos.", response = JsonEndereco.class, responseContainer = "List")
	public Response all() {
		Response response = null;
		try {
			List<JsonEndereco> jsonEnderecos = Parser.toListJsonEnderecos(enderecoService.all());
			response = Response.ok(jsonEnderecos).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar todos os registros[%s]", e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, null)).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Endereco's Page by comparation [LIKE] of query parameters and pagination parameters", response = Pager.class)
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "page", value = "With page", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "orderBy", value = "the sort field", paramType = "query", dataType="string"),  			
		@ApiImplicitParam(name = "order", value = "sort ASC or sort DESC", paramType = "query", dataType="string", allowableValues="asc,desc"),  			
		@ApiImplicitParam(name = "referencia", value = "Referencia", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "logradouro", value = "logradouro", paramType = "query", dataType="String"),  			
		@ApiImplicitParam(name = "cep", value = "Cep", paramType = "query", dataType="integer"),		
	})	
	public Response all(@Context UriInfo uriInfo) {
		Response response = null;
		Pager<Endereco> enderecos = null;

		try {
			PaginationParams<FilterEndereco> paginationParams = new PaginationParams<FilterEndereco>(uriInfo, FilterEndereco.class);
			enderecos = enderecoService.all(paginationParams);
			JsonPaginator<JsonEndereco> paginator = new JsonPaginator<JsonEndereco>(Parser.toListJsonEnderecos(enderecos.getItens()), enderecos.getActualPage(), enderecos.getTotalRecords());

			response = Response.ok(paginator).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar enderecos para os parametros %s [%s]", uriInfo.getQueryParameters().toString(), e.getMessage());
			LOGGER.error(message, e);
			response = Response.serverError().entity(new JsonError(e, message, uriInfo.getQueryParameters().toString())).build();
		}
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response get(@PathParam("id") Integer id) {
		try {

			Endereco endereco = enderecoService.get(id);

			return Response.ok().entity(Parser.toJson(endereco)).build();

		} catch (Exception e) {
			String message = String.format("Não foi possivel carregar o registro. [ %s ] parametros [ %d ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save a  Endereco.", response = JsonEndereco.class)
	public Response save(JsonEndereco jsonEndereco) {
		try {

			Endereco endereco = Parser.toEntity(jsonEndereco);
			endereco = enderecoService.save(endereco);
			return Response.ok().entity(Parser.toJson(endereco)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonEndereco.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonEndereco, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar  endereco [ %s ] parametros [ %s ]", e.getMessage(), jsonEndereco.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonEndereco)).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(value = "Update a Endereco. with the 'id' ", response = JsonEndereco.class)
	public Response update(@PathParam("id") Integer id, JsonEndereco jsonEndereco) {
		try {
			Endereco endereco = Parser.toEntity(jsonEndereco);

			endereco = enderecoService.save(endereco);
			return Response.ok().entity(Parser.toJson(endereco)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel salvar  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), jsonEndereco.toString());
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, jsonEndereco, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel salvar o registro [ %s ] parametros [ %s ]", e.getMessage(), jsonEndereco.toString());
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, jsonEndereco)).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the Endereco with the 'id' .", response = JsonEndereco.class)
	public Response delete(@PathParam("id") Integer id) {
		try {
			return Response.ok().entity(enderecoService.delete(id)).build();
		} catch (ValidationException e) {
			String message = String.format("Não foi possivel remover  o registro [ %s ] parametros [ %s ]", e.getOrigem().getMessage(), id);
			LOGGER.error(message, e.getOrigem());
			return Response.serverError().entity(new JsonError(e, message, id, e.getLegalMessage())).build();
		} catch (Exception e) {
			String message = String.format("Não foi possivel remover o registro [ %s ] parametros [ %s ]", e.getMessage(), id);
			LOGGER.error(message, e);
			return Response.serverError().entity(new JsonError(e, message, id)).build();
		}
	}
}
