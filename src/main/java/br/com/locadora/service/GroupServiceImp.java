package br.com.locadora.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.LocalDate;

import br.com.locadora.model.Group;
import br.com.locadora.persistence.DaoGroup;
import br.com.locadora.model.filter.FilterGroup;

import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.persistence.pagination.Pagination;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.core.utils.DateUtil;
import br.com.locadora.core.utils.Util;
/* generated by JSetup v0.95 :  at 08/09/2017 13:26:26 */

@Named
@Transactional
public class GroupServiceImp implements GroupService {

	private static final Logger LOGGER = Logger.getLogger(GroupServiceImp.class);
	
	@Inject
	DaoGroup daoGroup;

	@Override
	public Group get(Integer id) {
		return daoGroup.find(id);
	}
	
	@Override
	public Pager<Group> all(PaginationParams paginationParams) {
		Pagination<Group> pagination = daoGroup.getAll(paginationParams);
		return new Pager<Group>(pagination.getResults(), 0, pagination.getTotalRecords());
	} 
	
	
	@Override
	public List<Group> filter(PaginationParams paginationParams,  Boolean equals) {
		List<Group> list = daoGroup.filter(paginationParams, equals);
		return list;
	}
	
	@Override
	public List<Group> filter(FilterGroup filterGroup ,  Boolean equals) {
		List<Group> list = daoGroup.filter(filterGroup, equals);
		return list;
	}
	
	@Override
	public List<Group> all() {
		return daoGroup.getAll();
	}

	@Override
	public List<Group> search(String description) {
		return new ArrayList<Group>();
	}
	
	public List<Group> last(LocalDateTime lastSyncDate){
		return daoGroup.last(lastSyncDate);
	}
			
	@Override
	public Group save(Group entity) {
		return daoGroup.save(entity);
	}

	@Override
	public Group update(Group entity) {
		return daoGroup.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		Boolean del = Boolean.FALSE;
		try {
			del = daoGroup.delete(id);
		} catch (ConstraintViolationException e) {
			throw new ValidationException(e, "Não é possível remover um registro que já está sendo utilizado por outro.");
		}
		return del;			
	}
	
	
	
}
//generated by JSetup v0.95 :  at 08/09/2017 13:26:26