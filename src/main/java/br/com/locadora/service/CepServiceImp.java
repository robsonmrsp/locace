package br.com.locadora.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.LocalDate;
import br.com.locadora.core.model.CustomerOwner ;

import br.com.locadora.model.Cep;
import br.com.locadora.persistence.DaoCep;
import br.com.locadora.model.filter.FilterCep;

import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.persistence.pagination.Pagination;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.core.utils.DateUtil;
import br.com.locadora.core.utils.Util;
/* generated by JSetup v0.95 :  at 08/09/2017 13:26:25 */

@Named
@Transactional
public class CepServiceImp implements CepService {

	private static final Logger LOGGER = Logger.getLogger(CepServiceImp.class);
	
	@Inject
	DaoCep daoCep;

	@Override
	public Cep get(Integer id) {
		return daoCep.find(id);
	}
	
	@Override
	public Pager<Cep> all(PaginationParams paginationParams) {
		Pagination<Cep> pagination = daoCep.getAll(paginationParams);
		return new Pager<Cep>(pagination.getResults(), 0, pagination.getTotalRecords());
	} 
	
	
	@Override
	public List<Cep> filter(PaginationParams paginationParams,  Boolean equals) {
		List<Cep> list = daoCep.filter(paginationParams, equals);
		return list;
	}
	
	@Override
	public List<Cep> filter(FilterCep filterCep ,  Boolean equals) {
		List<Cep> list = daoCep.filter(filterCep, equals);
		return list;
	}
	
	@Override
	public List<Cep> all() {
		return daoCep.getAll();
	}

	@Override
	public List<Cep> search(String description) {
		return new ArrayList<Cep>();
	}
	
	public List<Cep> last(LocalDateTime lastSyncDate){
		return daoCep.last(lastSyncDate);
	}
			
	@Override
	public Cep save(Cep entity) {
		return daoCep.save(entity);
	}

	@Override
	public Cep update(Cep entity) {
		return daoCep.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		Boolean del = Boolean.FALSE;
		try {
			del = daoCep.delete(id);
		} catch (ConstraintViolationException e) {
			throw new ValidationException(e, "Não é possível remover um registro que já está sendo utilizado por outro.");
		}
		return del;			
	}
	
	@Override
	public Cep get(Integer id, CustomerOwner  client) {
		return daoCep.find(id, client);
	}

	@Override
	public List<Cep> all(CustomerOwner  client) {
		return daoCep.getAll(client);
	}

	@Override
	public Pager<Cep> all(PaginationParams paginationParams, CustomerOwner  owner) {
		Pagination<Cep> pagination = daoCep.getAll(paginationParams, owner);
		return new Pager<Cep>(pagination.getResults(), paginationParams.getPage(), pagination.getTotalRecords());
	}
	
		@Override
	public List<Cep> filter(PaginationParams paginationParams, CustomerOwner  owner) {
		List<Cep> list = daoCep.filter(paginationParams, owner);
		return list;
	}
	
	@Override
	public List<Cep> filter(PaginationParams paginationParams,  CustomerOwner  owner, Boolean equals) {
		List<Cep> list = daoCep.filter(paginationParams, owner, equals);
		return list;
	}
	
	@Override
	public List<Cep> filter(FilterCep filterCep , CustomerOwner  owner,  Boolean equals) {
		List<Cep> list = daoCep.filter(filterCep, owner, equals);
		return list;
	}
	
	
}
//generated by JSetup v0.95 :  at 08/09/2017 13:26:25