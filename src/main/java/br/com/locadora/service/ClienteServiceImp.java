package br.com.locadora.service;

import java.util.List;
import org.apache.log4j.Logger;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.LocalDate;
import br.com.locadora.core.model.CustomerOwner ;

import br.com.locadora.model.Cliente;
import br.com.locadora.persistence.DaoCliente;
import br.com.locadora.model.filter.FilterCliente;

import br.com.locadora.core.persistence.pagination.Pager;
import br.com.locadora.core.rs.exception.ValidationException;
import br.com.locadora.core.persistence.pagination.Pagination;
import br.com.locadora.core.persistence.pagination.PaginationParams;
import br.com.locadora.core.utils.DateUtil;
import br.com.locadora.core.utils.Util;
/* generated by JSetup v0.95 :  at 08/09/2017 13:26:23 */

@Named
@Transactional
public class ClienteServiceImp implements ClienteService {

	private static final Logger LOGGER = Logger.getLogger(ClienteServiceImp.class);
	
	@Inject
	DaoCliente daoCliente;

	@Override
	public Cliente get(Integer id) {
		return daoCliente.find(id);
	}
	
	@Override
	public Pager<Cliente> all(PaginationParams paginationParams) {
		Pagination<Cliente> pagination = daoCliente.getAll(paginationParams);
		return new Pager<Cliente>(pagination.getResults(), 0, pagination.getTotalRecords());
	} 
	
	
	@Override
	public List<Cliente> filter(PaginationParams paginationParams,  Boolean equals) {
		List<Cliente> list = daoCliente.filter(paginationParams, equals);
		return list;
	}
	
	@Override
	public List<Cliente> filter(FilterCliente filterCliente ,  Boolean equals) {
		List<Cliente> list = daoCliente.filter(filterCliente, equals);
		return list;
	}
	
	@Override
	public List<Cliente> all() {
		return daoCliente.getAll();
	}

	@Override
	public List<Cliente> search(String description) {
		return new ArrayList<Cliente>();
	}
	
	public List<Cliente> last(LocalDateTime lastSyncDate){
		return daoCliente.last(lastSyncDate);
	}
			
	@Override
	public Cliente save(Cliente entity) {
		return daoCliente.save(entity);
	}

	@Override
	public Cliente update(Cliente entity) {
		return daoCliente.save(entity);
	}

	@Override
	public Boolean delete(Integer id) {
		Boolean del = Boolean.FALSE;
		try {
			del = daoCliente.delete(id);
		} catch (ConstraintViolationException e) {
			throw new ValidationException(e, "Não é possível remover um registro que já está sendo utilizado por outro.");
		}
		return del;			
	}
	
	@Override
	public Cliente get(Integer id, CustomerOwner  client) {
		return daoCliente.find(id, client);
	}

	@Override
	public List<Cliente> all(CustomerOwner  client) {
		return daoCliente.getAll(client);
	}

	@Override
	public Pager<Cliente> all(PaginationParams paginationParams, CustomerOwner  owner) {
		Pagination<Cliente> pagination = daoCliente.getAll(paginationParams, owner);
		return new Pager<Cliente>(pagination.getResults(), paginationParams.getPage(), pagination.getTotalRecords());
	}
	
		@Override
	public List<Cliente> filter(PaginationParams paginationParams, CustomerOwner  owner) {
		List<Cliente> list = daoCliente.filter(paginationParams, owner);
		return list;
	}
	
	@Override
	public List<Cliente> filter(PaginationParams paginationParams,  CustomerOwner  owner, Boolean equals) {
		List<Cliente> list = daoCliente.filter(paginationParams, owner, equals);
		return list;
	}
	
	@Override
	public List<Cliente> filter(FilterCliente filterCliente , CustomerOwner  owner,  Boolean equals) {
		List<Cliente> list = daoCliente.filter(filterCliente, owner, equals);
		return list;
	}
	
	
}
//generated by JSetup v0.95 :  at 08/09/2017 13:26:23