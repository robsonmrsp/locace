/* generated: 12/06/2017 14:33:43 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var JSetup = require('views/components/JSetup');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var PermissionModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/permissions',

		defaults : {
			id: null,
	    	name : '',    	
	    	description : '',    	
	    	operation : '',    	
	    	tagReminder : '',    	
			roles : null,
			groups : null,
			item : null,
		
		}
	});
	return PermissionModel;
});
