/* generated: 12/06/2017 14:33:41 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var JSetup = require('views/components/JSetup');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FuncionarioModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/funcionarios',

		defaults : {
			id: null,
	    	celular : '',    	
	    	rg : '',    	
	    	dataHoraDemissao : '',    	
	    	dataHoraContratacao : '',    	
	    	dataNascimento : '',    	
	    	salario : '',    	
	    	observacao : '',    	
	    	ativo : '',    	
	    	telefone : '',    	
	    	cpf : '',    	
	    	nome : '',    	
	    	foto : '',    	
	    	percentualComissao : '',    	
			locacoes : null,
		
		}
	});
	return FuncionarioModel;
});
