/* generated: 12/06/2017 14:33:41 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var JSetup = require('views/components/JSetup');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var ClassificacaoModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/classificacaos',

		defaults : {
			id: null,
	    	descricao : '',    	
	    	idadeMinima : '',    	
	    	nome : '',    	
			filmes : null,
		
		}
	});
	return ClassificacaoModel;
});
