/* generated: 12/06/2017 14:33:42 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var JSetup = require('views/components/JSetup');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var InformacaoFilmeModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/informacaoFilmes',

		defaults : {
			id: null,
	    	lancamento : '',    	
	    	percentualDesconto : '',    	
	    	dataCompra : '',    	
	    	quantidadeEstoque : '',    	
	    	valor : '',    	
	    	selo : '',    	
			filme : null,
		
		}
	});
	return InformacaoFilmeModel;
});
