/* generated: 12/06/2017 14:33:41 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var JSetup = require('views/components/JSetup');	
	// End of "Import´s definition"

	// #####################################################################################################
	// ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ MAIN BODY  ¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨
	// #####################################################################################################

	var FilmeModel = JSetup.BaseModel.extend({

		urlRoot : 'rs/crud/filmes',

		defaults : {
			id: null,
	    	tituloOriginal : '',    	
	    	poster : '',    	
	    	duracao : '',    	
	    	dataLancamento : '',    	
	    	sinopse : '',    	
	    	titulo : '',    	
			generos : null,
			classificacao : null,
			linguagem : null,
			informacaoFilmes : null,
			reservas : null,
			atores : null,
			locacoes : null,
		
		}
	});
	return FilmeModel;
});
