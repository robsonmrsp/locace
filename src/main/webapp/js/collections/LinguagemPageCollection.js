/* generated: 12/06/2017 14:33:42 */
define(function(require) {
	// Start "Import´s" Definition"
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var Backbone = require('adapters/backbone-adapter');
	var LinguagemModel = require('models/LinguagemModel');
	// End of "Import´s definition"
	
	var LinguagemsCollection = Backbone.PageableCollection.extend({

		model : LinguagemModel,

		
		url : 'rs/crud/linguagems',

		mode : 'server',
		state : {
			firstPage : 1,
			lastPage : null,
			currentPage : 1,
			pageSize :10,
			totalPages : null,
			totalRecords : null,
			sortKey : null,
			order : -1
		},

		queryParams : {
			totalPages : null,
			pageSize : "pageSize",
			totalRecords : null,
			sortKey : "orderBy",
			order : "direction",
			directions : {
				"-1" : "asc",
				"1" : "desc"
			}
		},
	});

	return LinguagemsCollection;
});