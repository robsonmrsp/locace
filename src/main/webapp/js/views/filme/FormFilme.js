/* generated by JSetup v0.95 :  at 08/09/2017 13:26:25 */
define(function(require) {
	var _ = require('adapters/underscore-adapter');
	var $ = require('adapters/jquery-adapter');
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');

	var TemplateFormFilmes = require('text!views/filme/tpl/FormFilmeTemplate.html');
	
	var Filme = require('models/Filme');
	var Linguagem = require('models/Linguagem');			
	var Genero = require('models/Genero');			
	var Classificacao = require('models/Classificacao');			
	
	var FormFilmes = JSetup.View.extend({
		template : _.template(TemplateFormFilmes),

		/** The declared form Regions. */
		regions : {
			uploadPosterRegion : '.poster-container',		
		},

		/** The form events you'd like to listen */
		events : {
			'click 	.save' : 'save',
			'click 	.save-continue' : 'saveAndContinue',
		},
		/** All the important fields must be here. */
		ui : {
			inputId : '#inputId',
			saveButton : '.button-saving',
			inputTituloOriginal : '#inputTituloOriginal',
			inputPoster : '#inputPoster',
			inputDuracao : '#inputDuracao',
			inputDataLancamento : '#inputDataLancamento',
			groupInputDataLancamento : '#groupInputDataLancamento',
			inputSinopse : '#inputSinopse',
			inputTitulo : '#inputTitulo',
			inputLinguagem : '#inputLinguagem', 
			inputGeneros : '#inputGeneros', 
			inputClassificacao : '#inputClassificacao', 
			form : '#formFilme',
		},
		
		/** First function called, like a constructor. */
		initialize : function(options) {
			var that = this;
			//here the code 
		},
		
		/** Called after DOM´s ready.*/
		onShowView : function() {
			var that = this;
			this.ui.inputDuracao.integer();
			this.ui.inputDataLancamento.date();
			this.ui.groupInputDataLancamento.date();

			this.comboLinguagem = new JSetup.Combobox({
				el : this.ui.inputLinguagem,
				comboId : 'id',
				comboVal : 'nome',
				collectionEntity : Linguagem.Collection, 
				initialValue : that.model.get('linguagem'),					
			});
			this.multiselectGeneros = new JSetup.Multiselect({
				el : this.ui.inputGeneros,
				comboId : 'id',
				comboVal : 'nome',
				collectionEntity : Genero.Collection, 
			    initialValue : that.model.get('generos'),
			});
			this.comboClassificacao = new JSetup.Combobox({
				el : this.ui.inputClassificacao,
				comboId : 'id',
				comboVal : 'nome',
				collectionEntity : Classificacao.Collection, 
				initialValue : that.model.get('classificacao'),					
			});
			this.uploadViewPoster = new JSetup.InputUpload({
				bindElement : that.ui.inputPoster,
				onSuccess : function(resp, options) {
					console.info('Upload da poster concluido...[ ' + resp + ' ]')
				},
				onError : function(resp, options) {
					console.error('Problemas ao uppar foto1')
				}
			});
		
			this.uploadPosterRegion.show(this.uploadViewPoster);		
		},
		
		saveAndContinue : function() {
			this.save(true)
		},

		save : function(continua) {
			var that = this;
			var filme = that.getModel();

			if (this.isValid()) {
			this.ui.saveButton.button('loading');
				filme.save({}, {
					success : function(_model, _resp, _options) {
						util.showSuccessMessage('Filme salvo com sucesso!');
						that.clearForm();

						if (continua != true) {
							util.goPage('app/filmes', true);
						}
					},

					error : function(_model, _resp, _options) {
						that.ui.saveButton.button('reset');
						util.showErrorMessage('Problema ao salvar registro',_resp);
					}
				});
			} 
		},
		
		getModel : function() {
			var that = this;
			var filme = that.model; 
			filme.set({
				id: this.ui.inputId.escape(),
		    	tituloOriginal : this.ui.inputTituloOriginal.escape(), 
		    	poster : this.ui.inputPoster.escape(), 
		    	duracao : this.ui.inputDuracao.escape(true),
		    	dataLancamento : this.ui.inputDataLancamento.escape(), 
		    	sinopse : this.ui.inputSinopse.escape(), 
		    	titulo : this.ui.inputTitulo.escape(), 
				
				linguagem :  that.comboLinguagem.getJsonValue(),
				generos :  that.multiselectGeneros.getJsonValue(),
				classificacao :  that.comboClassificacao.getJsonValue(),
			});
			return filme;
		},
		
		customClearForm : function(){
			this.ui.saveButton.button('reset');
			this.uploadViewPoster.clear();		
		    this.comboLinguagem.clear(); 
		    this.multiselectGeneros.clear(); 
		    this.comboClassificacao.clear(); 
		},
	});

	return FormFilmes;
});