/* generated by JSetup v0.95 :  at 08/09/2017 13:26:25 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');
	var Ator = require('models/Ator');
	
	var ModalAtorTemplate = require('text!views/modalComponents/tpl/ModalAtorTemplate.html');


	var AtorModal = JSetup.View.extend({
		template : _.template(ModalAtorTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableAtorRegion : '.datatable-ator',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click .btnSearchAtor' : 'searchAtor',
			'click .btnClearAtor' : 'clearModal',
			'click tr' : 'selectRow',
			'keypress' : 'treatKeypress',
		},

		/** All the important fields must be here. */		
		ui : {
			loadButton : '.button-loading',
    		inputModalBiografia : '.inputModalBiografia',
    		inputModalDataNascimento : '.inputModalDataNascimento',
			groupInputModalDataNascimento : '.groupInputModalDataNascimento',
    		inputModalNome : '.inputModalNome',
		
			form : '#formSearchAtor',
			modalScreen : '.modal',
		},
		treatKeypress : function (e){
		    if (util.enterPressed(e)) {
	    		e.preventDefault();
	    		this.searchAtor();
	    	}
		},

		/** First function called, like a constructor. */
		initialize : function(opt) {
			var that = this;

			this.onSelectModel = opt.onSelectModel;
			this.suggestConfig = opt.suggestConfig;

			this.atorCollection = new Ator.PageCollection();
			this.atorCollection.state.pageSize = 5;
			this.atorCollection.on('fetching', this.startFetch, this);
			this.atorCollection.on('fetched', this.stopFetch, this);
			
			this.dataTableAtor = new JSetup.DataTable({
				row : JSetup.RowClick,
				columns : this.getColumns(),
				collection : this.atorCollection,
			});
			this.setValue(opt.initialValue);
		},
		
		/** Called after DOM´s ready.*/
		onShowView :  function() {
			var that = this;
		  	
		  	
			this.ui.inputModalDataNascimento.date();
			this.ui.groupInputModalDataNascimento.date();
		  	
		  	
	
			that.dataTableAtorRegion.show(this.dataTableAtor);
					
			if (that.suggestConfig) {
				that.suggestConfig.collection = that.atorCollection;
				that.suggestConfig.onSelect = function(json) {
					var model = new JSetup.BaseModel(json)
					that.onSelectModel(model);
					if (json) {
						that.modelSelect = model
					} else
						that.modelSelect = null;
				}
				util.configureSuggest(that.suggestConfig);
				that.suggestConfig.field.change(function(e) {
					if(!that.suggestConfig.field.val()){
						that.clear();
					}
				})
			}		
		},

		searchAtor : function() {
			var that = this;
			this.atorCollection.filterQueryParams = {
	    		biografia : this.ui.inputModalBiografia.escape(), 
	    		dataNascimento : this.ui.inputModalDataNascimento.escape(), 
	    		nome : this.ui.inputModalNome.escape(), 
			};

			this.atorCollection.fetch({
				resetState : true,
				success : function(_coll, _resp, _opt) {
					//caso queira algum tratamento de sucesso adicional
				},
				error : function(_coll, _resp, _opt) {
					console.error(_coll, _resp, _opt)
				}
			});
		},

		selectRow : function(e) {
			var modelAtor = util.getWrappedModel(e);
			if (modelAtor){
				this.modelSelect = modelAtor; 
				this.onSelectModel(modelAtor);
			}
		},
		
		getJsonValue : function() {
			if (_.isEmpty(this.modelSelect) && _.isEmpty(this.jsonValue)) {
				return null;
			}
			if (this.modelSelect) {
				return this.modelSelect.toJSON();
			} else {
				return this.jsonValue;
			}
		},
		
		getRawValue : function() {
			var json = this.getJsonValue();
			if(json )
				return json.id
			return null;
		},
		
		getValue : function() {
			return this.modelSelect;
		},

		setValue : function(value) {
			this.jsonValue = value;
		},

		getColumns : function() {
			var columns = [	

					{
				name : "biografia",
				sortable : true,
				editable : false,
				label 	 : "Biografia",
				cell : JSetup.CustomStringCell
			}, 
					{
				name : "dataNascimento",
				sortable : true,
				editable : false,
				label 	 : "Data nascimento",
				cell : JSetup.CustomStringCell
			}, 
					{
				name : "nome",
				sortable : true,
				editable : false,
				label 	 : "Nome",
				cell : JSetup.CustomStringCell
			}, 
						];
			return columns;
		},

		hidePage : function() {
			this.ui.modalScreen.modal('hide');
		},

		showPage : function() {
			this.clearModal();
			this.ui.modalScreen.modal('show');
			
			this.searchAtor();
		},

		clear : function() {
			this.clearModal();
		},
		clearModal : function() {
			this.modelSelect = null;
			this.jsonValue = null;
			this.atorCollection.reset();
			util.scrollUpModal();
			this.ui.form.get(0).reset();
		},
		
		// Executada depois da consulta concluida.
		stopFetch : function() {
			this.ui.loadButton.button('reset');
			util.stopSpinner();
			util.scrollDownModal();
		},
		
		// Executada Antes da realização da consulta.
		startFetch : function() {
			this.ui.loadButton.button('loading');
			util.showSpinner('spinAtor');
		},
	});

	return AtorModal;
});
