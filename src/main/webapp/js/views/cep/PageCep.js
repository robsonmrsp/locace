/* generated by JSetup v0.95 :  at 08/09/2017 13:26:25 */
define(function(require) {
	var util = require('utilities/utils');
	var JSetup = require('views/components/JSetup');	
	var PageCepTemplate = require('text!views/cep/tpl/PageCepTemplate.html');

	var Cep = require('models/Cep');

	var PageCep = JSetup.View.extend({
		template : _.template(PageCepTemplate),

		/** The declared form Regions. */
		regions : {
			dataTableCepRegion : '.datatable-cep',
		},
		
		/** The form events you'd like to listen */
		events : {
			'click 	.reset-button' : 'resetCep',			
			'keypress' : 'treatKeypress',
			
			'click 	.search-button' : 'searchCep',
			'click  .show-advanced-search-button' : 'toggleAdvancedForm',
		},
		
		/** All the important fields must be here. */		
		ui : {
			loadButton : '.loading-button',
			form : '#formCepFilter',
			advancedSearchForm : '.advanced-search-form',
		},
		
		/** First function called, like a constructor. */		
		initialize : function() {
			var that = this;
			this.ceps = new Cep.PageCollection();
			this.ceps.on('fetching', this.startFetch, this);
			this.ceps.on('fetched', this.stopFetch, this);
			
			this.dataTableCep = new JSetup.DataTable({
				columns : this.getColumns(),
				collection : this.ceps
			});
		},

		/** Called after DOM´s ready.*/		
		onShowView : function() {
			var that = this;
			this.dataTableCepRegion.show(that.dataTableCep);
			this.searchCep();
		},
		 
		searchCep : function(){
			var that = this;
			util.loadButton(this.ui.loadButton)
			this.ceps.filterQueryParams = {
			}
			this.ceps.fetch({
				success : function(_coll, _resp, _opt) {
					//console.info('Consulta para o grid cep');
				},
				error : function(_coll, _resp, _opt) {
					//console.error(_resp.responseText || (_resp.getResponseHeader && _resp.getResponseHeader('exception')));
				},
			})		
		},
		resetCep : function(){
			this.ui.form.get(0).reset();
			this.ceps.reset();
		},
				
		getColumns : function() {
			var that = this;
			var columns = [
			{
				name : "acoes",
				label : "Ações(Editar, Deletar)",
				editable : false,
				sortable : false,
				cell : JSetup.ActionCell.extend({
					buttons : that.getCellButtons(),
					context : that,
				})
			} ];
			return columns;
		},
		
		getCellButtons : function() {
			var that = this;
			var buttons = [];

			buttons.push({
				id : 'edit_button',
				type : 'primary',
				icon : 'fa-pencil',
				customClass : 'auth[edit-cep,disable]',
				hint : 'Editar Cep',
				onClick : that.editModel,
			}, {
				id : 'delete_button',
				type : 'danger',
				customClass : 'auth[delete-cep, disable]',
				icon : 'fa-trash',
				hint : 'Remover Cep',
				onClick : that.deleteModel,
			});

			return buttons;
		},

		deleteModel : function(model) {
			var that = this;
			
			var modelTipo = new Cep.Model({id : model.id});
			
			util.confirm({
				title : "Importante",
				text : "Tem certeza que deseja remover o registro [ " + model.get('id') + " ] ?",
				onConfirm : function() {
					modelTipo.destroy({
						success : function() {
							that.ceps.remove(model);
							util.alert({title : "Concluido", text : "Registro removido com sucesso!"});
						},
					});
				}
			});
		},

		editModel : function(model) {
			util.goPage("app/editCep/" + model.get('id'));
		},

		
		// adictional functions
		toggleAdvancedForm : function() {
			this.ui.advancedSearchForm.slideToggle("slow");
		},

		treatKeypress : function(e) {
			if (util.enterPressed(e)) {
				e.preventDefault();
				this.searchCep();
			}
		},
		startFetch : function() {
			util.loadButton(this.ui.loadButton)
		},

		stopFetch : function() {
			util.resetButton(this.ui.loadButton)
		}
	});

	return PageCep;
});
