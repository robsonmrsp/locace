define(function(require) {
	var Router = require('Router');
	describe("Rotas", function() {

		beforeEach(function() {
			try {
				Backbone.history.stop();
			} catch (e) {
				console.error(e);
			}
		});
		
		afterEach(function() {
			// Reset URL
			var router = new Router();
			router.navigate("");
		});
				it("Rota de \"Clientes\"", function() {
			spyOn(Router.prototype, "Clientes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Clientes', true);
			expect(Router.prototype.Clientes).toHaveBeenCalled();
		});

		it("Rota de \"newcliente\"", function() {
			spyOn(Router.prototype, "newcliente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcliente', true);
			expect(Router.prototype.newcliente).toHaveBeenCalled();
		});
		
		it("Rota de \"editcliente\"", function() {
			spyOn(Router.prototype, "editcliente")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcliente/1', true);
			expect(Router.prototype.editcliente).toHaveBeenCalled();
		});
		it("Rota de \"Ators\"", function() {
			spyOn(Router.prototype, "Ators")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Ators', true);
			expect(Router.prototype.Ators).toHaveBeenCalled();
		});

		it("Rota de \"newator\"", function() {
			spyOn(Router.prototype, "newator")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newator', true);
			expect(Router.prototype.newator).toHaveBeenCalled();
		});
		
		it("Rota de \"editator\"", function() {
			spyOn(Router.prototype, "editator")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editator/1', true);
			expect(Router.prototype.editator).toHaveBeenCalled();
		});
		it("Rota de \"Funcionarios\"", function() {
			spyOn(Router.prototype, "Funcionarios")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Funcionarios', true);
			expect(Router.prototype.Funcionarios).toHaveBeenCalled();
		});

		it("Rota de \"newfuncionario\"", function() {
			spyOn(Router.prototype, "newfuncionario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newfuncionario', true);
			expect(Router.prototype.newfuncionario).toHaveBeenCalled();
		});
		
		it("Rota de \"editfuncionario\"", function() {
			spyOn(Router.prototype, "editfuncionario")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editfuncionario/1', true);
			expect(Router.prototype.editfuncionario).toHaveBeenCalled();
		});
		it("Rota de \"Filmes\"", function() {
			spyOn(Router.prototype, "Filmes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Filmes', true);
			expect(Router.prototype.Filmes).toHaveBeenCalled();
		});

		it("Rota de \"newfilme\"", function() {
			spyOn(Router.prototype, "newfilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newfilme', true);
			expect(Router.prototype.newfilme).toHaveBeenCalled();
		});
		
		it("Rota de \"editfilme\"", function() {
			spyOn(Router.prototype, "editfilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editfilme/1', true);
			expect(Router.prototype.editfilme).toHaveBeenCalled();
		});
		it("Rota de \"Linguagems\"", function() {
			spyOn(Router.prototype, "Linguagems")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Linguagems', true);
			expect(Router.prototype.Linguagems).toHaveBeenCalled();
		});

		it("Rota de \"newlinguagem\"", function() {
			spyOn(Router.prototype, "newlinguagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newlinguagem', true);
			expect(Router.prototype.newlinguagem).toHaveBeenCalled();
		});
		
		it("Rota de \"editlinguagem\"", function() {
			spyOn(Router.prototype, "editlinguagem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editlinguagem/1', true);
			expect(Router.prototype.editlinguagem).toHaveBeenCalled();
		});
		it("Rota de \"Classificacaos\"", function() {
			spyOn(Router.prototype, "Classificacaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Classificacaos', true);
			expect(Router.prototype.Classificacaos).toHaveBeenCalled();
		});

		it("Rota de \"newclassificacao\"", function() {
			spyOn(Router.prototype, "newclassificacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newclassificacao', true);
			expect(Router.prototype.newclassificacao).toHaveBeenCalled();
		});
		
		it("Rota de \"editclassificacao\"", function() {
			spyOn(Router.prototype, "editclassificacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editclassificacao/1', true);
			expect(Router.prototype.editclassificacao).toHaveBeenCalled();
		});
		it("Rota de \"Locacaos\"", function() {
			spyOn(Router.prototype, "Locacaos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Locacaos', true);
			expect(Router.prototype.Locacaos).toHaveBeenCalled();
		});

		it("Rota de \"newlocacao\"", function() {
			spyOn(Router.prototype, "newlocacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newlocacao', true);
			expect(Router.prototype.newlocacao).toHaveBeenCalled();
		});
		
		it("Rota de \"editlocacao\"", function() {
			spyOn(Router.prototype, "editlocacao")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editlocacao/1', true);
			expect(Router.prototype.editlocacao).toHaveBeenCalled();
		});
		it("Rota de \"InformacaoFilmes\"", function() {
			spyOn(Router.prototype, "InformacaoFilmes")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/InformacaoFilmes', true);
			expect(Router.prototype.InformacaoFilmes).toHaveBeenCalled();
		});

		it("Rota de \"newinformacaoFilme\"", function() {
			spyOn(Router.prototype, "newinformacaoFilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newinformacaoFilme', true);
			expect(Router.prototype.newinformacaoFilme).toHaveBeenCalled();
		});
		
		it("Rota de \"editinformacaoFilme\"", function() {
			spyOn(Router.prototype, "editinformacaoFilme")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editinformacaoFilme/1', true);
			expect(Router.prototype.editinformacaoFilme).toHaveBeenCalled();
		});
		it("Rota de \"Enderecos\"", function() {
			spyOn(Router.prototype, "Enderecos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Enderecos', true);
			expect(Router.prototype.Enderecos).toHaveBeenCalled();
		});

		it("Rota de \"newendereco\"", function() {
			spyOn(Router.prototype, "newendereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newendereco', true);
			expect(Router.prototype.newendereco).toHaveBeenCalled();
		});
		
		it("Rota de \"editendereco\"", function() {
			spyOn(Router.prototype, "editendereco")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editendereco/1', true);
			expect(Router.prototype.editendereco).toHaveBeenCalled();
		});
		it("Rota de \"Ceps\"", function() {
			spyOn(Router.prototype, "Ceps")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Ceps', true);
			expect(Router.prototype.Ceps).toHaveBeenCalled();
		});

		it("Rota de \"newcep\"", function() {
			spyOn(Router.prototype, "newcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newcep', true);
			expect(Router.prototype.newcep).toHaveBeenCalled();
		});
		
		it("Rota de \"editcep\"", function() {
			spyOn(Router.prototype, "editcep")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editcep/1', true);
			expect(Router.prototype.editcep).toHaveBeenCalled();
		});
		it("Rota de \"Reservas\"", function() {
			spyOn(Router.prototype, "Reservas")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Reservas', true);
			expect(Router.prototype.Reservas).toHaveBeenCalled();
		});

		it("Rota de \"newreserva\"", function() {
			spyOn(Router.prototype, "newreserva")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newreserva', true);
			expect(Router.prototype.newreserva).toHaveBeenCalled();
		});
		
		it("Rota de \"editreserva\"", function() {
			spyOn(Router.prototype, "editreserva")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editreserva/1', true);
			expect(Router.prototype.editreserva).toHaveBeenCalled();
		});
		it("Rota de \"Generos\"", function() {
			spyOn(Router.prototype, "Generos")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Generos', true);
			expect(Router.prototype.Generos).toHaveBeenCalled();
		});

		it("Rota de \"newgenero\"", function() {
			spyOn(Router.prototype, "newgenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newgenero', true);
			expect(Router.prototype.newgenero).toHaveBeenCalled();
		});
		
		it("Rota de \"editgenero\"", function() {
			spyOn(Router.prototype, "editgenero")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editgenero/1', true);
			expect(Router.prototype.editgenero).toHaveBeenCalled();
		});
		it("Rota de \"Users\"", function() {
			spyOn(Router.prototype, "Users")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Users', true);
			expect(Router.prototype.Users).toHaveBeenCalled();
		});

		it("Rota de \"newuser\"", function() {
			spyOn(Router.prototype, "newuser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newuser', true);
			expect(Router.prototype.newuser).toHaveBeenCalled();
		});
		
		it("Rota de \"edituser\"", function() {
			spyOn(Router.prototype, "edituser")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edituser/1', true);
			expect(Router.prototype.edituser).toHaveBeenCalled();
		});
		it("Rota de \"Roles\"", function() {
			spyOn(Router.prototype, "Roles")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Roles', true);
			expect(Router.prototype.Roles).toHaveBeenCalled();
		});

		it("Rota de \"newrole\"", function() {
			spyOn(Router.prototype, "newrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newrole', true);
			expect(Router.prototype.newrole).toHaveBeenCalled();
		});
		
		it("Rota de \"editrole\"", function() {
			spyOn(Router.prototype, "editrole")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editrole/1', true);
			expect(Router.prototype.editrole).toHaveBeenCalled();
		});
		it("Rota de \"Permissions\"", function() {
			spyOn(Router.prototype, "Permissions")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Permissions', true);
			expect(Router.prototype.Permissions).toHaveBeenCalled();
		});

		it("Rota de \"newpermission\"", function() {
			spyOn(Router.prototype, "newpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newpermission', true);
			expect(Router.prototype.newpermission).toHaveBeenCalled();
		});
		
		it("Rota de \"editpermission\"", function() {
			spyOn(Router.prototype, "editpermission")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editpermission/1', true);
			expect(Router.prototype.editpermission).toHaveBeenCalled();
		});
		it("Rota de \"Groups\"", function() {
			spyOn(Router.prototype, "Groups")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Groups', true);
			expect(Router.prototype.Groups).toHaveBeenCalled();
		});

		it("Rota de \"newgroup\"", function() {
			spyOn(Router.prototype, "newgroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newgroup', true);
			expect(Router.prototype.newgroup).toHaveBeenCalled();
		});
		
		it("Rota de \"editgroup\"", function() {
			spyOn(Router.prototype, "editgroup")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/editgroup/1', true);
			expect(Router.prototype.editgroup).toHaveBeenCalled();
		});
		it("Rota de \"Items\"", function() {
			spyOn(Router.prototype, "Items")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/Items', true);
			expect(Router.prototype.Items).toHaveBeenCalled();
		});

		it("Rota de \"newitem\"", function() {
			spyOn(Router.prototype, "newitem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/newitem', true);
			expect(Router.prototype.newitem).toHaveBeenCalled();
		});
		
		it("Rota de \"edititem\"", function() {
			spyOn(Router.prototype, "edititem")
			var router = new Router();
			Backbone.history.start();
			router.navigate('app/edititem/1', true);
			expect(Router.prototype.edititem).toHaveBeenCalled();
		});
	});
})
