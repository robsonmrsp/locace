package br.com.locace.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.locadora.model.Filme;
import br.com.locadora.model.Role;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:test-beans.xml", "classpath:test-security.xml" })
@SpringBootTest(classes = { Application.class }, webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql({ "classpath:init-data.sql" })
public class FilmeControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	// @BeforeClass
	// public static void statup() {
	//
	// SecurityContextHolder.getContext().setAuthentication());
	// }

	@Test
	public void testAddEmployee() throws Exception {
		// Test code that uses the above RestTemplate ...

		// prepare
		Filme employee = new Filme();

		String plainCreds = "jsetup:123456";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<Role> responseEntity = restTemplate.exchange("/rs/crud/roles/3", HttpMethod.GET, request, Role.class);

		// execute
		// ResponseEntity<Filme> responseEntity =
		// restTemplate.getForEntity("/rs/crud/filmes", Filme.class);

		// collect Response
		int status = responseEntity.getStatusCodeValue();
		Role resultEmployee = responseEntity.getBody();

		// verify
		assertEquals("Incorrect Response Status", HttpStatus.OK.value(), status);

		assertNotNull(resultEmployee);
		// assertNotNull(resultEmployee.getId().longValue());
	}
}
